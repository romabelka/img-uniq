const sharp = require('sharp')
const imageInfo = require('imageinfo')
const path = require('path')

module.exports = class UniqImg {
    constructor(image) {
        this.image = sharp(image)
        this.info = imageInfo(image)
    }

    run() {
        this.resize()
        this.extract()
        this.extend()
        this.gammaCorrection()
        this.blur()
        this.sharpen()
        this.overlay()
        return this.image
    }

    resize() {
        const {width, height} = this.info
        this.image = this.image.resize(width + 100, height + 100)
        return this
    }

    extract() {
        const cropSize = 10
        this.image.extract({left: cropSize / 2, top: cropSize / 2,
            width: this.info.width - cropSize, height: this.info.height - cropSize})
    }

    extend() {
        const extra = 5
        const colorCode = 157
        const alpha = 0.5

        this.image = this.image
            .background({r: colorCode, g: colorCode, b: colorCode, alpha})
            .extend({top: extra, bottom: extra, left: extra, right: extra})
        return this
    }

    gammaCorrection() {
        const gamma = 1.01//1 - 1.02
        this.image = this.image.gamma(gamma)
        return this
    }

    sharpen() {
        const sigma = 2//1 - 2
        this.image = this.image.sharpen(sigma)
        return this
    }

    blur() {
        const sigma = 1//0.3 - 1000
        this.image = this.image.blur(sigma)
        return this
    }

    overlay() {
        const overlayImg = sharp(path.resolve(__dirname, '../bitcoin.png'))
//        this.image.overlayWith(path.resolve(__dirname, '../bitcoin.png'))
        return this
    }
}

//info
/*
{ type: 'image',
    format: 'PNG',
    mimeType: 'image/png',
    width: 708,
    height: 816
}*/
