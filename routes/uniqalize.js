const express = require('express')
const router = express.Router()
const upload = require('multer')()
const sharp = require('sharp')
const Uniq = require('../services/uniq-img')

router.post('/', upload.single('file'), function (req, res, next) {
    res.set({
        'Content-Type': req.file.mimetype,
//      'Content-Transfer-Encoding': 'binary',
        'Content-Disposition': `attachment; filename="${req.file.originalname}"`
    });

    const uniq = new Uniq(req.file.buffer)
    uniq.run().pipe(res)

/*
    sharp(req.file.buffer)
        .resize(200, 300)
        .pipe(res)
*/
})

module.exports = router
